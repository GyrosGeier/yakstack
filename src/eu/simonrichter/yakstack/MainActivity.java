package eu.simonrichter.yakstack;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

public class MainActivity extends Activity
{
	private Context context;
	private TextView countView;
	private BroadcastReceiver countReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			countView.setText(
				String.valueOf(
					intent.getIntExtra(DatabaseService.EXTRA_ITEM_COUNT, 0)));
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.main);

		context = this;
		countView = (TextView)findViewById(R.id.titlebar);
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(countReceiver, new IntentFilter(DatabaseService.ACTION_GET_ITEM_COUNT));

		Intent getCountIntent = new Intent(context, DatabaseService.class);
		getCountIntent.setAction(DatabaseService.ACTION_GET_ITEM_COUNT);
		context.startService(getCountIntent);
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(countReceiver);
	}
}
