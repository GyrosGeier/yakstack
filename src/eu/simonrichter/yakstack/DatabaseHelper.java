package eu.simonrichter.yakstack;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import eu.simonrichter.yakstack.DatabaseContract.*;

public class DatabaseHelper extends SQLiteOpenHelper {
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "YakStack.db";

	public static final String SQL_CREATE_TODO_ITEMS =
		"CREATE TABLE " + TodoItems.TABLE_NAME + " (" +
		TodoItems._ID + " INTEGER PRIMARY KEY, " +
		TodoItems.COLUMN_NAME_TITLE + " TEXT, " +
		TodoItems.COLUMN_NAME_DESCRIPTION + " TEXT)";

	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(SQL_CREATE_TODO_ITEMS);
	}
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) { }
}
