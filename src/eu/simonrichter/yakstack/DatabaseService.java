package eu.simonrichter.yakstack;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import eu.simonrichter.yakstack.DatabaseContract.*;

public class DatabaseService extends IntentService {
	public DatabaseService() {
		super("YakStack Database Service");
	}

	public static final String ACTION_GET_ITEM_COUNT = "eu.simonrichter.yakstack.action.GET_ITEM_COUNT";

	public static final String EXTRA_ITEM_COUNT = "eu.simonrichter.yakstack.extra.ITEM_COUNT";

	private SQLiteDatabase db = null;

	public void onDestroy() {
		if(db != null)
			db.close();
	}

	public void onHandleIntent(Intent intent) {
		if(db == null)
		{
			DatabaseHelper helper = new DatabaseHelper(
					getApplicationContext());
			db = helper.getWritableDatabase();
		}

		if(intent != null) {
			final String action = intent.getAction();
			if(ACTION_GET_ITEM_COUNT.equals(action)) {
				final String[] columns = { "COUNT(*)" };
				Cursor cur = db.query(
						TodoItems.TABLE_NAME,
						columns,
						null,
						null,
						null,
						null,
						null,
						null);
				cur.moveToFirst();
				int count = cur.getInt(0);
				cur.close();

				Intent backIntent = new Intent(ACTION_GET_ITEM_COUNT);
				backIntent.putExtra(EXTRA_ITEM_COUNT, count);
				sendBroadcast(backIntent);
			}
		}
	}
}
