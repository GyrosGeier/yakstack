package eu.simonrichter.yakstack;

import android.provider.BaseColumns;

public final class DatabaseContract {
	private DatabaseContract() { }

	public static class TodoItems implements BaseColumns {
		public static final String TABLE_NAME = "todo_items";
		public static final String COLUMN_NAME_TITLE = "title";
		public static final String COLUMN_NAME_DESCRIPTION = "description";
	}
}
