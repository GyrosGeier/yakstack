#! /bin/sh -ex

PACKAGE=eu.simonrichter.yakstack
VERSION="`git describe --always`"
SHORTNAME=yakstack
MANIFEST=AndroidManifest.xml

JAVA_VERSION=1.6
ANDROID_JAR=/usr/lib/android-sdk/platforms/android-23/android.jar

JAVAC=javac
JARSIGNER=jarsigner
DX=/usr/lib/android-sdk/build-tools/debian/dx
AAPT=aapt
ZIPALIGN=zipalign

AAPT_FLAGS_COMMON="-f -M $MANIFEST -S res -I $ANDROID_JAR --version-name $VERSION"
AAPT_FLAGS_RELEASE=""
AAPT_FLAGS_DEBUG="--debug-mode --rename-manifest-package $PACKAGE.debug"

if [ "$1" = "release" ]; then
	AAPT_FLAGS="$AAPT_FLAGS_COMMON $AAPT_FLAGS_RELEASE"
else
	AAPT_FLAGS="$AAPT_FLAGS_COMMON $AAPT_FLAGS_DEBUG"
fi

rm -rf gen
mkdir gen

$AAPT package $AAPT_FLAGS -J gen/ -m
$JAVAC -cp $ANDROID_JAR -source $JAVA_VERSION -target $JAVA_VERSION -d gen -s gen `find src gen -name *.java`
$DX --output=gen/classes.dex --dex gen/
$AAPT package $AAPT_FLAGS -F gen/$SHORTNAME.apk.unsigned
(cd gen && $AAPT add $SHORTNAME.apk.unsigned classes.dex)

$JARSIGNER -signedjar gen/$SHORTNAME.apk.unaligned gen/$SHORTNAME.apk.unsigned "mykey"
$ZIPALIGN 4 gen/$SHORTNAME.apk.unaligned gen/$SHORTNAME.apk
